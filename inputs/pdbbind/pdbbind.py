from pathlib import Path

import pandas as pd
import numpy as np


def read_pdbbind(pdbbind_file: Path) -> pd.DataFrame:
    """Parser for PDBBind

    Args:
        pdbbind_file: Path to INDEX_general_PL_data.2020 file
    Returns:
        df: clean df
    """

    with open(pdbbind_file, "r") as fl:
        data = fl.readlines()

    table = [ln.strip().split() for ln in data[6:]]
    col_names = [
        "pdb",
        "resolution",
        "release_year",
        "p_activity",
        "activity",
        "_",
        "reference",
        "ligand",
        "_",
    ]

    df = pd.DataFrame(table, columns=col_names).drop(columns=["_", "reference"])
    df["pdb"] = df["pdb"].apply(lambda x: x.upper())
    df["ligand"] = df["ligand"].str.replace("(", "").str.replace(")", "")
    df[['assay', 'value_units']] = df["activity"].str.split("<=|>=|=|~|>|<", expand=True)
    df["value"] = df["value_units"].str.extract('(\d*\.\d+|\d+)').astype(float)
    df["units"] = df["value_units"].str.extract(r'\(?([A-Za-z]+)\)?')
    df = df.drop(columns=['value_units'])
    col_order = [
        "pdb",
        "ligand",
        "resolution",
        "release_year",
        "activity",
        "assay",
        "value",
        "units",
        "p_activity",
    ]
    return df[col_order]
