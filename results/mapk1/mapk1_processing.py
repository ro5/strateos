from copy import deepcopy
from pathlib import Path
from typing import List

import numpy as np
import pandas as pd
import tqdm


def name_to_label(name: str) -> str:
    return name.split("_")[2]


def parse_decaf(df: pd.DataFrame, inclusion: List[str]) -> pd.DataFrame:

    df = df.drop(columns=["reference_conformer", "query_conformer"])
    df = df.rename(columns={"decaf_score": "score"})
    df["score"] = df["score"].astype(np.float16)
    df = df[~df["reference"].str.contains("_stereo")]
    # df = df[df["reference"].isin(inclusion)]
    return df


def preprocess_bio2d(df: pd.DataFrame) -> pd.DataFrame:
    df = df.drop(columns=["smiles"])
    df["label"] = df["name"].apply(name_to_label)
    df = df.rename(columns={"qsar_2d_predictions": "score"})
    df["hypothesis"] = "bio2d"
    return df


def preprocess_decaf_public(df: pd.DataFrame):
    df = df.drop(columns=["ref_p_activity"])
    df = df.rename(columns={"reference": "hypothesis"})
    df["label"] = df["name"].apply(name_to_label)
    # Remove stereoisomers
    df = df[df["name"].apply(lambda x: len(x.split("_")) == 3)]
    df = update_ranks(df)
    df["conservative_label"] = label_coding(df["label"], method="conservative")
    df["liberal_label"] = label_coding(df["label"], method="liberal")
    return df


def preprocess_general(df: pd.DataFrame):
    df_melt = df.melt(id_vars=["name"], var_name="hypothesis", value_name="score")
    df_melt["label"] = df_melt["name"].apply(name_to_label)
    # Remove stereoisomers
    df_melt = df_melt[df_melt["name"].apply(lambda x: len(x.split("_")) == 3)]
    df_melt["hypothesis"] = df_melt["hypothesis"].apply(lambda x: x.replace("_ligand", ""))
    df_melt = update_ranks(df_melt)
    df_melt["conservative_label"] = label_coding(df_melt["label"], method="conservative")
    df_melt["liberal_label"] = label_coding(df_melt["label"], method="liberal")
    return df_melt


def preprocess_pharmit(df: pd.DataFrame) -> pd.DataFrame:
    df["label"] = df["name"].apply(name_to_label)
    # Remove stereoisomers
    df = df[df["name"].apply(lambda x: len(x.split("_")) == 3)]
    df = df.sort_values(["ph4_auc"], ascending=False)
    df["conformer_rank"] = df.groupby(["name", "hypothesis"]).cumcount() + 1
    df = df[df["conformer_rank"] == 1]
    df = df[["name", "hypothesis", "label", "ph4_auc"]]
    df = df.rename(columns={"ph4_auc": "score"})
    df = update_ranks(df)
    df["conservative_label"] = label_coding(df["label"], method="conservative")
    df["liberal_label"] = label_coding(df["label"], method="liberal")

    return df


def update_ranks(df: pd.DataFrame) -> pd.DataFrame:
    df = df.sort_values(["score"], ascending=False)
    df = df.reset_index(drop=True)
    df["method_rank"] = (
        df.index + 1
    ).values  # TODO: This does not take into accound that the same hit might be refered to several times.
    df["hypothesis_rank"] = df.groupby(["hypothesis"]).cumcount() + 1
    df["compound_rank"] = df.groupby(["name"]).cumcount() + 1

    return df


def label_coding(labels: pd.Series, method="conservative") -> pd.Series:

    assert method in ["conservative", "liberal"]

    if method == "conservative":
        mapping = {"Inactive": 0, "Inconclusive": 0, "Active": 1}
    else:
        mapping = {"Inactive": 0, "Inconclusive": 1, "Active": 1}

    return labels.replace(mapping)


def read_data(method: str, data_dir: Path):
    # process each .csv into a desirable form

    if method == "decaf":
        df = pd.read_csv(data_dir / "mapk1_shape_decaf.csv")
        df = preprocess_general(df)
        return df
    elif method == "usrcat":
        df = pd.read_csv(data_dir / "mapk1_shape_usrcat.csv")  # mapk1_shape_usrcat_oneconf.csv
        df = preprocess_general(df)
        return df
    elif method == "tanimoto":
        df = pd.read_csv(data_dir / "mapk1_tanimoto.csv")
        df = df.drop(columns=["base_index", "Max_Response", "smiles", "label"])
        df = preprocess_general(df)
        return df
    elif method == "pharmit":
        df = pd.read_csv(data_dir / "mapk1_pharmit.csv")
        df = preprocess_pharmit(df)
        return df
    elif method == "bio2d":
        df = pd.read_csv(data_dir / "mapk1_hts_from_public_bio2d.csv")
        df = preprocess_bio2d(df)
        return df
    elif method == "decaf_public":
        df = pd.read_csv(data_dir / "mapk1_hts_public_final.csv")
        df = preprocess_decaf_public(df)
        return df
    else:
        raise Exception(f"Unknown method: {method}!")


def get_joint_df(
    data_dir: Path,
    methods: List[str] = ["usrcat", "decaf", "pharmit", "tanimoto", "decaf_public"],
) -> pd.DataFrame:
    all_tables = {method: read_data(method, data_dir=data_dir) for method in methods}
    joint_df = pd.concat(all_tables, names=["method"]).reset_index()
    joint_df = joint_df.drop(columns=['level_1'])

    return joint_df
